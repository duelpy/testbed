"""An implementation of axiomatic assumption checks on preference matrices.

This module implements checks for axiomatic assumptions. The assumptions were described by [1]_. Given a preference
matrix, this module can perform checks for those assumptions.

References
----------
.. [1] Busa-Fekete, Robert, Eyke Hüllermeier, and Adil El Mesaoudi-Paul. "Preference-based online learning with dueling
bandits: A survey." arXiv preprint arXiv:1807.11398 (2018)
"""
# Tested and verified with the examples present in [1]
from itertools import permutations
import math
from typing import List
from typing import Optional

import numpy as np


class PreferenceMatrix:
    """Implements checks for axiomatic assumptions on preference matrices.

    Parameters
    ----------
    preference_matrix
        The preference matrix to check. This assumes a fixed, finite number of arms and with finite
        dimension of the preference matrix.

    Attributes
    ----------
    preference_matrix
        The preference matrix represented by this class.
    calibrated_matrix
        A calibrated version of the preference matrix such that 0 describes maximal uncertainty (adjusted by (-0.5)).
    """

    def __init__(self, preference_matrix: np.array) -> None:
        self.preference_matrix = preference_matrix
        if (
            self.preference_matrix.ndim != 2
        ):  # preference_matrix has to be a matrix, i.e. a 2d array.
            raise ValueError(
                f"Preference matrices have to be matrices, i.e. 2 dimensional arrays. The input matrix has "
                f"{self.preference_matrix.ndim} dimensions."
            )
        rows, columns = np.shape(self.preference_matrix)
        if (
            self.preference_matrix.ndim == 2 and rows != columns
        ):  # 2d preference_matrix has to be square matrix
            raise ValueError(
                f"Preference matrices have to be square matrices. The input matrix has {rows} rows and "
                f"{columns} columns, which is not square."
            )
        # Check that probabilities add to one
        for row in range(rows):
            for column in range(columns):
                sum_of_probability = (
                    self.preference_matrix[row][column]
                    + self.preference_matrix[column][row]
                )
                if not math.isclose(sum_of_probability, 1):
                    raise ValueError(
                        f"The probability that any arms wins and the probability that it loses have to "
                        f"add up to 1. That is not the case for arms {column} and {row}, where the sum is "
                        f"{sum_of_probability}"
                    )
        self.calibrated_matrix = self.preference_matrix - 0.5
        self.no_of_arms = len(self.preference_matrix)  # total number of arms
        self.arms = list(range(self.no_of_arms))  # arms

    def is_low_noise_model(self) -> bool:
        """Check if the preference matrix adheres to the low noise assumption.

        Returns
        -------
        bool
            True if the preference matrix models a low noise problem.
        """
        for arm_i in self.arms:
            for arm_j in self.arms:
                if arm_i == arm_j:
                    continue
                calibrated_probability_value = self.calibrated_matrix[arm_i][arm_j]
                # One arm has to be expected to win. The low noise model assumption is violated if there is draw
                # between two arms. Between the two chosen arms, the winner arm and the loser arm is decided based on
                # the calibrated probability.
                if calibrated_probability_value == 0:
                    return False
                elif calibrated_probability_value > 0:
                    winner_arm, loser_arm = (arm_i, arm_j)
                else:
                    winner_arm, loser_arm = (arm_j, arm_i)
                # Stores sum of the calibrated probability of arms with respect to winner and loser arm respectively
                calibrated_probability_sum_reference_winner = 0
                calibrated_probability_sum_reference_loser = 0
                for arm_k in self.arms:
                    calibrated_probability_sum_reference_winner += self.calibrated_matrix[
                        winner_arm
                    ][
                        arm_k
                    ]
                    calibrated_probability_sum_reference_loser += self.calibrated_matrix[
                        loser_arm
                    ][
                        arm_k
                    ]
                # Sum of the calibrated probability of arms with respect to winner should always be greater than sum of
                # calibrated probability of arms with respect to loser arm.
                if (
                    calibrated_probability_sum_reference_winner
                    <= calibrated_probability_sum_reference_loser
                ):
                    return False
        return True

    def get_total_order(self) -> Optional[List[int]]:
        """Get an ordering of the arms that respects the preference relation.

        Any arm at position `i` is expected to win against any arm at position `j > i`.

        Returns
        -------
        Optional[List[int]]
            The list of arms in an order that respects the preference relation (a arm with a lower
            index is expected to beat any arm with a higher index), if such a total order exists.
            None if total order does not exist.
        """
        total_order = []

        def compute_total_order_recursive(arm_set: set) -> None:
            nonlocal total_order
            # check for the empty set
            for chosen_arm in arm_set:
                arms_excluding_chosen_arm = arm_set.difference([chosen_arm])
                # Assume the chosen arm wins against all other arms
                chosen_arm_wins = True
                for (
                    other_arm
                ) in (
                    arms_excluding_chosen_arm
                ):  # Check whether the chosen arm wins against all other arms
                    if self.calibrated_matrix[chosen_arm][other_arm] <= 0:
                        chosen_arm_wins = False
                if chosen_arm_wins:
                    total_order.append(chosen_arm)
                    total_order_before_length = len(total_order)
                    compute_total_order_recursive(arms_excluding_chosen_arm)
                    # Total order length only changes when one chosen arm was added at every recursive step
                    total_order_after_length = len(total_order)
                    if (
                        len(arms_excluding_chosen_arm) != 0
                        and total_order_before_length == total_order_after_length
                    ):
                        # No total order possible with chosen arm at this position
                        total_order.pop()

        # Execute the inner function
        compute_total_order_recursive(set(self.arms))
        if len(total_order) == 0:
            return None
        return total_order

    def has_total_order(self) -> bool:
        """Check if a total order exists for the preference matrix.

        Returns
        -------
        bool
            True if a total orders arms exists for the given preference matrix.
        """
        return self.get_total_order() is not None

    def has_strong_stochastic_transitivity(self) -> bool:
        """Check whether the strong stochastic transitivity (SST) property holds for the given preference matrix.

        Returns
        -------
        bool
            True if preference matrix holds strong stochastic transitivity.
        """
        for (arm_i, arm_j, arm_k) in permutations(self.arms, 3):
            if (
                self.calibrated_matrix[arm_i][arm_j] >= 0
                and self.calibrated_matrix[arm_j][arm_k] >= 0
                and self.calibrated_matrix[arm_i][arm_k]
                < np.maximum(
                    self.calibrated_matrix[arm_i][arm_j],
                    self.calibrated_matrix[arm_j][arm_k],
                )
            ):
                return False
        return True

    def has_relaxed_stochastic_transitivity(self, gamma: float) -> bool:
        """Check whether the preference matrix holds the relaxed stochastic transitivity (gamma RST).

        Parameters
        ----------
        gamma
            constant value in a range between 0 and 1.

        Returns
        -------
        bool
            True if preference matrix holds gamma relaxed stochastic transitivity for a user provided gamma value.

        Raises
        ------
        ValueError
            If gamma value is not in a range between 0 and 1.
        """
        if gamma <= 0 or gamma >= 1:
            raise ValueError(
                f"provided gamma value {gamma} should in the range between 0 and 1"
            )
        for (arm_i, arm_j, arm_k) in permutations(self.arms, 3):
            if (
                self.calibrated_matrix[arm_i][arm_j] >= 0
                and self.calibrated_matrix[arm_j][arm_k] >= 0
                and self.calibrated_matrix[arm_i][arm_k]
                < gamma
                * np.maximum(
                    self.calibrated_matrix[arm_i][arm_j],
                    self.calibrated_matrix[arm_j][arm_k],
                )
            ):
                return False
        return True

    def has_moderate_stochastic_transitivity(self) -> bool:
        """Check whether the preference matrix hold the moderate stochastic transitivity(MST) property.

        Returns
        -------
        bool
            True if preference matrix holds moderate stochastic transitivity.
        """
        for (arm_i, arm_j, arm_k) in permutations(self.arms, 3):
            if (
                self.calibrated_matrix[arm_i][arm_j] >= 0
                and self.calibrated_matrix[arm_j][arm_k] >= 0
                and self.calibrated_matrix[arm_i][arm_k]
                < np.minimum(
                    self.calibrated_matrix[arm_i][arm_j],
                    self.calibrated_matrix[arm_j][arm_k],
                )
            ):
                return False
        return True

    def has_weak_stochastic_transitivity(self) -> bool:
        """Check whether the given preference matrix has the weak stochastic transitivity (WST) property.

        Returns
        -------
        bool
            True if preference matrix holds weak stochastic transitivity.
        """
        for (arm_i, arm_j, arm_k) in permutations(self.arms, 3):
            if (
                self.calibrated_matrix[arm_i, arm_j] >= 0
                and self.calibrated_matrix[arm_j, arm_k] >= 0
            ):
                if self.calibrated_matrix[arm_i, arm_k] < 0:
                    return False
        return True

    def has_stochastic_triangle_inequality(self) -> bool:
        """Check whether the given three arms hold the stochastic triangle inequality (STI) property.

        Returns
        -------
        bool
            True if preference matrix holds stochastic triangle inequality (STI).
        """
        #   STI is only defined when total order exist.
        total_order = self.get_total_order()
        if total_order is None:
            return False
        # Get all the permutation of arm_i, arm_j, arm_k
        for arm_i, arm_j, arm_k in permutations(self.arms, 3):
            # Filter triples that need to be checked according the the STI premise
            if (
                total_order.index(arm_i) >= total_order.index(arm_j)
                or total_order.index(arm_i) >= total_order.index(arm_k)
                or total_order.index(arm_j) >= total_order.index(arm_k)
            ):
                continue
            #   Condition check for STI
            if self.calibrated_matrix[arm_i][arm_k] > (
                self.calibrated_matrix[arm_i][arm_j]
                + self.calibrated_matrix[arm_j][arm_k]
            ):
                return False
        return True

    def get_condorcet_winner(self) -> Optional[int]:
        """Get the condorcet winner if present in the given preference matrix.

        Returns
        -------
        condorcet_winner
            arm as a integer value, if condorcet winner exist.
            None if condorcet winner does not exist
        """
        condorcet_winner = None
        for arm in self.arms:
            arm_calibration = np.asarray(self.calibrated_matrix[arm])
            arm_calibration = np.delete(
                arm_calibration, arm
            )  # Arm calibration with itself is not required
            if np.amin(arm_calibration) > 0:
                condorcet_winner = arm
                return condorcet_winner
        return condorcet_winner

    def has_condorcet_winner(self) -> bool:
        """Check whether the given preference has condorcet winner of not.

        Returns
        -------
        bool
            True if condorcet winner exist.
        """
        return self.get_condorcet_winner() is not None

    def has_general_identifiability_assumption(self) -> bool:
        """Check whether the General Identifiability Assumption holds.

        Returns
        -------
        bool
            True if there exist a arm that hold General identiyability assumption.
        """
        # GIA implies condorcet winner. so, condorcet winner should exist for GIA check.
        condorcet_winner = self.get_condorcet_winner()
        if condorcet_winner is None:
            return False
        for arm in self.arms:
            for other_arm in self.arms:
                if (
                    self.calibrated_matrix[arm][other_arm]
                    > self.calibrated_matrix[condorcet_winner][other_arm]
                ):
                    return False
        return True


def _main() -> None:
    # Implementation for the example of proper uses of functionality of the module.
    preference_matrix = np.array(
        [[0.5, 0, 0.6, 0.6], [1, 0.5, 1, 1], [0.4, 0, 0.5, 1], [0.4, 0, 0, 0.5]]
    )

    test_object = PreferenceMatrix(preference_matrix)
    print("1.preference matrix:\n", test_object.preference_matrix)
    print("2.calibrated matrix:\n", test_object.calibrated_matrix)
    print("3.number of arms: ", test_object.no_of_arms)
    print("4.arms: ", test_object.arms)
    print("5.low noise model: ", test_object.is_low_noise_model())
    print("6.total order exist: ", test_object.has_total_order())
    print("7.total order of arms: ", test_object.get_total_order())
    print(
        "8.strong stochastic transitivity (SST): ",
        test_object.has_strong_stochastic_transitivity(),
    )
    print(
        "9.gamma relaxed stochastic transitivity (gamma-RST): ",
        test_object.has_relaxed_stochastic_transitivity(0.4),
    )
    print(
        "10.moderate stochastic transitivity (MST): ",
        test_object.has_moderate_stochastic_transitivity(),
    )
    print(
        "11.weak stochastic transitivity (WST): ",
        test_object.has_weak_stochastic_transitivity(),
    )
    print(
        "12.stochastic triangle inequality: ",
        test_object.has_stochastic_triangle_inequality(),
    )
    print(
        "14.general_general_identifiability_assumption:",
        test_object.has_general_identifiability_assumption(),
    )
    print("15.condorcet winner exist: ", test_object.has_condorcet_winner())
    print("16.condorcet winner is: ", test_object.has_condorcet_winner())


if __name__ == "__main__":
    _main()
