"""An implementation of the weak regret version of the 'Winner Stays' algorithm."""

from typing import Callable
from typing import Tuple

import numpy as np


class WinnerStays:
    """Implements the weak regret version of the "Winner Stays" algorithm.

    This algorithm is tournament based and minimizes the expected regret of the best arm.
    It assumes at the very least that a Condorcet winner exists, but the expected regret improves if the arms can additionally be ranked.
    It stores the difference between won and lost rounds for each arm. The next actions are then selected from the set of arms with the highest difference.
    If one of the actions from the previous round is still in this argmax set, it is chosen again.
    In the first round and if the actions of the previous round are not part of the argmax set, the actions are chosen uniformly random from it.
    The two chosen actions are guaranteed to be not identical.

    Parameters
    ----------
    arm_count
        The number of arms available to the algorithm. This assumes a fixed, finite number of arms.
    feedback
        A function taking two arm indices as an input and returning the preference. 0 or larger means the first arm won, else the second arm won.

    Attributes
    ----------
    win_deltas
        Stores the difference between won and lost rounds for each arm. Corresponds to the C(t,i) values in [1]_.
    feedback
        The function used for gathering the feedback of drawing arms.
    random_state
        Optional, used for random choices in the algorithm.

    References
    ----------
    .. [1] Chen, Bangrui, and Peter I. Frazier. "Dueling bandits with weak regret." Proceedings of the 34th International Conference on Machine Learning-Volume 70. JMLR. org, 2017.
    """

    def __init__(
        self,
        arm_count: int,
        feedback: Callable[[int, int], int],
        random_state: np.random.RandomState = np.random.RandomState(),
    ) -> None:
        self.win_deltas = np.zeros((arm_count,))
        self.feedback = feedback
        self._last_arm_i = -1
        self._last_arm_j = -1
        self.random_state = random_state

    def round(self) -> None:
        """Execute one round of the algorithm.

        Run through one round of the algorithm. First, the arms with the largest wins-losses difference are selected. Then the feedback is used to update the win-loss statistic.
        """
        # could maybe use an iterator for the round, which stops if the time horizon is reached?

        # selection of arms i,j
        # arm i
        arm_i_candidates = _argmax(self.win_deltas)

        if self._last_arm_i in arm_i_candidates:
            arm_i = self._last_arm_i
        elif self._last_arm_j in arm_i_candidates:
            arm_i = self._last_arm_j
        else:
            arm_i = self.random_state.choice(arm_i_candidates)

        # arm j
        # make sure we find the argmax without i and remove i if it is still in the argmax
        c_without_i = np.delete(self.win_deltas, arm_i)
        c_argmax = _argmax(c_without_i)
        arm_j_candidates = np.delete(c_argmax, np.argwhere(c_argmax == arm_i))

        if self._last_arm_i in arm_j_candidates:
            arm_j = self._last_arm_i
        elif self._last_arm_j in arm_j_candidates:
            arm_j = self._last_arm_j
        else:
            arm_j = self.random_state.choice(arm_j_candidates)

        self._last_arm_i = arm_i
        self._last_arm_j = arm_j

        # updating win-lose differences for the chosen arms
        if self.feedback(arm_i, arm_j) < 0:
            winner, loser = arm_j, arm_i
        else:
            winner, loser = arm_i, arm_j

        self.win_deltas[winner] += 1
        self.win_deltas[loser] -= 1


def _argmax(array: np.array) -> np.array:
    # np.argmax returns the first index, to get the whole set we search for all indices which point to a value equal to the maximum
    max_value = array.max()
    indices = np.argwhere(array == max_value)
    return indices.flatten()


def main() -> None:
    """Test the algorithm implementation.

    As we have no benchmarking/environment packages yet, a specific example is used here.
    """
    # arm 1 should be the condorcet winner, the arms can also be ranked as 1,2,3,4
    preference_matrix = np.array(
        [
            [0.5, 0.9, 0.9, 0.9],
            [0.1, 0.5, 0.75, 0.75],
            [0.1, 0.25, 0.5, 0.6],
            [0.1, 0.25, 0.4, 0.5],
        ]
    )

    history = []

    def test_feedback(i: int, j: int) -> int:
        """Calculate feedback to test the algorithm.

        The winner is chosen according to the probability in the preference matrix.

        Parameters
        ----------
        i
            The index of the first arm.
        j
            The index of the second arm.

        Returns
        -------
        feedback
            The feedback, either positive if `i` won, or negative if `j` won.
        """
        rand = np.random.uniform()
        winner = i if rand < preference_matrix[i, j] else j
        feedback = 1 if winner == i else -1

        history.append((i, j, feedback))
        return feedback

    # run the algorithm
    ws_object = WinnerStays(4, test_feedback)

    time_horizon = 20

    for _ in range(time_horizon):
        ws_object.round()  # alternative: ws.round(T)?

    # analyze regret
    def calculate_weak_regret(
        history: list, preference_matrix: np.array, best_arm_idx: int
    ) -> Tuple[list, float]:
        """Calculate the weak regret.

        The weak regret is the minimal distance between the chosen arms and the best arm.

        Parameters
        ----------
        history
            The history of chosen arms.
        preference_matrix
            The preference matrix for the given problem.
        best_arm_idx
            The index of the best arm.

        Returns
        -------
        regret_history
            A list containing the weak regret per round.
        cumul_regret
            The cumulative weak regret.
        """
        regret_history = []
        cumul_regret = 0
        for i, j, _ in history:
            weak_regret = (
                min(
                    preference_matrix[best_arm_idx, i],
                    preference_matrix[best_arm_idx, j],
                )
                - 0.5
            )
            regret_history.append(weak_regret)
            cumul_regret += weak_regret
        return regret_history, cumul_regret

    _, cumul_regret = calculate_weak_regret(history, preference_matrix, 0)
    print(f"Cumulative weak regret: {cumul_regret}")


if __name__ == "__main__":
    main()
