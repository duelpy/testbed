"""An implementation of the Relative Confidence Sampling algorithm."""
from typing import Callable
from typing import Optional
from typing import Tuple

import numpy as np


class RelativeConfidenceSampling:
    """An implementation of the Relative Confidence Sampling algorithm.

    The Relative Confidence Sampling algorithm is based on [1]_.
    The algorithm assumes that a Condorcet winner among the given arms exists.
    The goal of the algorithm is to reduce the cumulative regret as quickly as possible.
    This is done by conducting duels among the candidate arms and eliminating the sub-optimal
    arms based on the results of the prior duels. After conducting sufficient rounds, the
    algorithm would always choose the Condorcet winner to conduct a duel with itself. This
    would result in no more regret and thus the goal would be achieved.

    Relative Confidence Sampling works continuously in the following 3 steps:

    1. A simulated tournament is conducted among all the arms to obtain a champion.
    The tournament is based on sampling from a beta distribution. The beta distribution
    is parameterized on the results of the prior duels among the competing arms.
    The Condorcet winner of this simulated tournament is selected as the current champion.
    If no Condorcet winner exists, the arm that has been selected as the champion the least
    number of times is the new champion.

    2. A challenger which has the highest chance of winning against the current champion
    is obtained using the upper confidence bound. The upper confidence bound is based on the
    probability estimates of winning against the champion. These probability estimates can be
    derived for every arm using the results of the prior duels against the champion.

    3. A duel is conducted among the champion and the challenger and the results are stored.

    .. [1] Masrour Zoghi, Shimon A. Whiteson, Maarten de Rijke,
    and Remi Munos. Relative Confidence Sampling for Efficient
    On-line Ranker Evaluation. In Proceedings of ACM International
    Conference on Web Search and Data Mining (WSDM), pages 73–82, 2014b.

    Parameters
    ----------
    number_of_arms
        Total number of participating arms.
    feedback
        Function which conducts a duel among arms and returns the winner.
    exploratory_constant
        A parameter which is used in calculating the upper confidence bounds. The confidence
        radius grows proportional to the square root of this value. A higher upper confidence
        bound results in more exploration.
        The value of exploratory_constant must be greater than 0.5. Default value is 0.501
        which has been used in the experiments related to RCS in [1]_.
    random
        A numpy random state. Defaults to an unseeded state when not specified.


    Attributes
    ----------
    feedback
    number_of_arms
    exploratory_constant
    random

    """

    def __init__(
        self,
        number_of_arms: int,
        feedback: Callable[[int, int], int],
        exploratory_constant: float = 0.501,
        random: Optional[np.random.RandomState] = None,
    ) -> None:
        self.number_of_arms = number_of_arms
        self.feedback = feedback
        self.time_step = 0
        if exploratory_constant <= 0.5:
            raise ValueError("Value of exploratory constant must be greater than 0.5")
        self.exploratory_constant = exploratory_constant
        self.random = random if random is not None else np.random.RandomState()
        # Scoresheet maintains the no. of wins of each arm over the other in a duel
        self._scoresheet = np.zeros(
            (self.number_of_arms, self.number_of_arms), dtype=int
        )

        self._champion_chosen_frequency = np.zeros(self.number_of_arms, dtype=int)

    def step(self) -> None:
        """Run one round of the algorithm."""
        self.time_step += 1
        champion = self._run_simulated_tournament()
        challenger = self._select_challenger_for(champion)
        self._conduct_duel(champion, challenger)

    def run(self, rounds: int) -> None:
        """Run the algorithm for a given number of rounds.

        Parameters
        ----------
        rounds
            The number of rounds for which the algorithm is run.
        """
        for _ in range(rounds):
            self.step()

    def _run_simulated_tournament(self) -> int:
        """Run a simulated tournament among all arms to pick the champion.

        Returns the Condorcet winner of the tournament, if existent, as the
        champion. A Condorcet winner is an arm whose probability of beating
        every other arm in the tournament is at least 0.5.
        If no Condorcet winner exists, then the arm which was least chosen
        as a champion is returned as the current champion.

        Returns
        -------
        int
            The champion of the tournament.
        """
        preference_matrix = self._sample_preference_matrix()
        condorcet_winner = self._condorcet_winner(preference_matrix)

        champion = (
            condorcet_winner
            if condorcet_winner is not None
            else np.argmin(self._champion_chosen_frequency)
        )
        self._champion_chosen_frequency[champion] += 1

        return champion

    def _sample_preference_matrix(self) -> np.ndarray:
        """Sample a preference matrix based on a Beta distribution.

        The outcome is a sampled preference matrix where each pairwise preference is
        drawn from a beta-distribution which is parameterized on the results of prior
        duels. The results of the previous duels are obtained from the scoresheet.
        The scoresheet consists of the number of wins of each arm over every other arm,
        respectively, in all the previous duels.

        Returns
        -------
        np.ndarray
            A sample preference matrix which contains the probability of winning of
            arm i over arm j given by preference_matrix_sample[i][j].
        """
        # The diagonal values remain 0.5 whereas the other values change after sampling.
        preference_matrix_sample = np.full(
            (self.number_of_arms, self.number_of_arms), 0.5
        )

        for i in range(self.number_of_arms):
            for j in range(self.number_of_arms):
                # Fill upper triangle of matrix `preference_matrix_sample`
                if i < j:
                    # Use Beta distribution based on the current scoresheet for sampling
                    preference_matrix_sample[i][j] = self.random.beta(
                        self._scoresheet[i][j] + 1, self._scoresheet[j][i] + 1
                    )

        # Fill lower triangle of matrix `preference_matrix_sample` using upper triangle
        for i in range(self.number_of_arms):
            for j in range(self.number_of_arms):
                if i > j:
                    preference_matrix_sample[i][j] = 1 - preference_matrix_sample[j][i]

        return preference_matrix_sample

    def _condorcet_winner(self, preference_matrix: np.ndarray) -> Optional[int]:
        """Return the condorcet winner from the given preference matrix.

        A Condorcet winner is an arm whose probability of beating every other arm in the
        tournament is at least 0.5. Returns the condorcet winner from the given matrix,
        if existent, otherwise None.

        Parameters
        ----------
        preference_matrix
            A matrix of size [K * K] where K is the total number of arms participating in
            the tournament. The value in the position preference_matrix[i][j] denotes the
            probability of arm i beating arm j in a duel. Since the probabilities in the
            preference matrix follow a binomial distribution, it holds that
            preference_matrix[i][j] = 1 - preference_matrix[j][i]

        Returns
        -------
        Optional[int]
            Condorcet winner, if existent, otherwise None.
        """
        for i in range(self.number_of_arms):
            defeated_arms = 0
            for j in range(self.number_of_arms):
                if preference_matrix[i][j] >= 0.5:
                    defeated_arms += 1

            # Check if condorcet winnner exists
            if defeated_arms == self.number_of_arms:
                return i

        return None

    def _select_challenger_for(self, champion: int) -> int:
        """Pick the appropriate challenger for the selected champion.

        Determine the challenger that has the highest probability of winning against
        the champion based on the upper confidence bound on the probability estimates.
        These probability estimates can be derived from the scoresheet.

        Parameters
        ----------
        champion
            The arm which is selected as the champion.

        Returns
        -------
        int
            The arm which is selected as the challenger to the champion.
        """
        upper_confidence_bounds = [
            self._compute_upper_confidence_bound_for(arm_j, champion)
            for arm_j in range(self.number_of_arms)
        ]
        challenger = np.argmax(upper_confidence_bounds)
        return challenger

    def _compute_upper_confidence_bound_for(
        self, comparison_arm: int, reference_arm: int
    ) -> float:
        """Calculate the upper confidence bound of the preference probability.

        Calculate the upper confidence bound of the win probability of `reference_arm`
        against `comparison_arm`. These bounds are derived from the Hoeffding's inequality.
        `preference_estimate` is the estimated preference probability while `confidence_radius`
        is the radius derived by Hoeffding's inequality and then re-scaled by a factor of
        sqrt(2 * exploratory_constant) to add additional exploration.

        Parameters
        ----------
        comparison_arm
            The comparison arm.
        reference_arm
            The reference arm.

        Returns
        -------
        float
            Upper confidence bound of the win probability of `reference_arm` against `comparison_arm`
        """
        if comparison_arm == reference_arm:
            return 0.5

        no_of_duels = (
            self._scoresheet[comparison_arm][reference_arm]
            + self._scoresheet[reference_arm][comparison_arm]
        )
        preference_estimate = None
        confidence_radius = None

        # Check if any previous duel has been conducted between `comparison_arm` and `reference_arm`
        if no_of_duels > 0:
            preference_estimate = (
                self._scoresheet[comparison_arm][reference_arm] / no_of_duels
            )
            confidence_radius = np.sqrt(
                (self.exploratory_constant * np.log(self.time_step)) / no_of_duels
            )
        else:
            # Assign 1 (assume x/0 := 1)
            preference_estimate = 1
            confidence_radius = 1

        upper_confidence_bound = preference_estimate + confidence_radius

        return upper_confidence_bound

    def _conduct_duel(self, champion: int, challenger: int) -> None:
        """Conduct a duel among the given arms.

        Conduct a duel among the given arms using the callable `feedback`
        and update the scoresheet based on the result of the duel.

        Parameters
        ----------
        champion
            The reference arm or champion.
        challenger
            The comparison arm or challenger.
        """
        (winner, loser) = (
            (champion, challenger)
            if self.feedback(champion, challenger) >= 0
            else (challenger, champion)
        )

        self._scoresheet[winner][loser] += 1


# Copied Code from winnerstays.py for testing
if __name__ == "__main__":
    preference_matrix_ = np.array(
        [
            [0.5, 0.9, 0.9, 0.9],
            [0.1, 0.5, 0.75, 0.75],
            [0.1, 0.25, 0.5, 0.6],
            [0.1, 0.25, 0.4, 0.5],
        ]
    )
    history_ = []

    # Copy of winner stays algorithm analyze regret
    def calculate_weak_regret(
        history: list, preference_matrix: np.array, best_arm_idx: int
    ) -> Tuple[list, float]:
        """Calculate the weak regret.

        The weak regret is the minimal distance between the chosen arms and the best arm.

        Parameters
        ----------
        history : list
            Description
        preference_matrix : np.array
            Description
        best_arm_idx : int
            Description

        Returns
        -------
        Tuple[list, float]
            Tuple with regret history and cumulative regret.
        """
        regret_history = []
        cumul_regret = 0
        for i, j, _ in history:
            weak_regret = (
                min(
                    preference_matrix[best_arm_idx, i],
                    preference_matrix[best_arm_idx, j],
                )
                - 0.5
            )
            regret_history.append(weak_regret)
            cumul_regret += weak_regret
        return regret_history, cumul_regret

    # copy of winner stays algorithm test feedback
    def test_feedback(i: int, j: int) -> int:
        """Calculate feedback to test the algorithm.

        The winner is chosen according to the probability in the preference matrix.

        Parameters
        ----------
        i : int
            The index of the first arm.
        j : int
            The index of the second arm.

        Returns
        -------
        int
            The feedback, either positive if `i` won, or negative if `j` won.
        """
        rand = np.random.uniform()
        winner = i if rand < preference_matrix_[i, j] else j
        feedback = 1 if winner == i else -1
        history_.append((i, j, feedback))
        return feedback

    test_object = RelativeConfidenceSampling(4, test_feedback)
    test_object.run(100)

    _, cumul_r = calculate_weak_regret(history_, preference_matrix_, 0)
    print(f"Cumulative weak regret: {cumul_r}")
