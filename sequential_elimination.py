"""An implementation of Sequential Elimination algorithm."""

import math
from typing import Callable

import numpy as np


class SequentialElimination:
    r"""Find the epsilon maximum arm with quadratic complexity.

    This module implements Sequential Elimination algorithm which was introduced in [1]_ to find the most preferred arms of a preference-based multi armed bandit problem.
    The algorithm is an adaptation of Probably Approximately Correct estimation of Condorcet Winner. The resulting arm will be epsilon prefereable to other arms.
    The performance of the algorithm depends upon the starting arm(anchor arm). If the anchor arm is a good anchor, then performance will be optimal.
    Good anchor element means that if the element is not epsilon preferable to atmost m other arms. Refer to the following formulae below.
    .. math:: '\mid \{ e:e \in S \thinspace and \thinspace \tilde{p}_(_a_,_b_) > \epsilon \} \mid \thinspace \leq m'
     Where \tilde{p}_(_a_,_b_) is probability estimate that element a beats element b, e is all other elements from arm_set(S).

    Attributes
    ----------
    feedback
        function used for gathering the feedback of drawing arms.
    bias_lower
        algorithm eliminates an arm if the calibrated preference estimate is less than bias_lower.
    bias_upper
        algorithm eliminates an arm if the calibrated preference estimate is greater than bias_upper.
    confidence
        is used to determine the no. of iteration required to reach to the conclusion.

    .. [1] Falahatgar, Jain,Orlitsky, Pichapati, Ravindrakumar. "The Limits of Maxing, Ranking, and Preference Learning" (2018).
    """

    def __init__(
        self,
        feedback: Callable[[int, int], int],
        bias_lower: float,
        bias_upper: float,
        confidence: float,
    ) -> None:
        self.bias_upper = bias_upper
        self.bias_lower = bias_lower
        self.confidence = confidence
        self.feedback = feedback

    def find_winner_arms(self, arms_pool_original: list, anchor_arm: str) -> str:
        """Return the preferred arm after eliminating all the other arms.

        In each round, this method compares the current anchor element with other elements one by one using compare method.
        After every round, if the anchor element is beaten, it is replaced by the winning element.

        Parameters
        ----------
        arms_pool_original
            list of arms out of which best arm is to be found.
        anchor_arm
            arm from the arms_pool_original which the user has chosen to be the anchor arm.

        Returns
        -------
        str
            arm that beats all the other arms.
        """
        arms_pool_copy = (
            arms_pool_original.copy()
        )  # arms_pool_copy will hold the updated set of arms.
        arms_pool_copy.remove(
            anchor_arm
        )  # remove the anchor arm from the arms_pool_original.

        # the confidence is updated so that compare function would evaluate the number of rounds it needs to run.
        self.confidence = 2 * self.confidence / len(arms_pool_original)
        while len(arms_pool_copy) != 0:  # until all elements are processed.
            anchor_arm_internal = anchor_arm
            arms_eliminated = list()  # list of arms that are eliminated.

            # comparing arms in arms_pool_copy against anchor element one by one.
            for current_arm in arms_pool_copy:
                comparison_result = self.compare(
                    arms_pool_original.index(anchor_arm),
                    arms_pool_original.index(current_arm),
                )  # index of the arms is used as preference matrix has rows and columns with int labels.

                if comparison_result == -1:  # anchor arm cannot beat the current arm.
                    arms_eliminated.append(
                        current_arm
                    )  # add the current arm into the list of the eliminated arms.

                elif comparison_result == 1:  # anchor arm beats the current arm.
                    anchor_arm = current_arm  # updates the anchor arm.
                    arms_eliminated.append(
                        current_arm
                    )  # arm is eliminated so that in next round, it doesn't show up for comparison again.
                    break
            if (
                anchor_arm == anchor_arm_internal
            ):  # this means that anchor_arm beats all other arms in term of its calibrated preference estimate.
                break

            arms_pool_copy = list(
                set(arms_pool_copy) - set(arms_eliminated)
            )  # before starting another round, arm_set_copy is updated with all the elements which are not eliminated.

        return anchor_arm

    def compare(self, anchor_arm_index: int, current_arm_index: int) -> int:
        """Compare 2 arms number of times such that it returns comparison results with (1-delta) confidence. Delta is the confidence provided to the algorithm.

        The number of rounds is calculated according to the confidence, upper bias and lower bias given to the algorithm.
        Output is determined using the calibrated preference estimate of anchor arm beating current arm.
        Because we cannot compare calibrated_preference_estimate > bias_upper vs the same < bias_upper because of probability estimate, that's why we compare the calibrated_preference_estimate of anchor arm beating current arm > bias_upper vs the same < bias_lower.

        Parameters
        ----------
        anchor_arm_index
            index of the anchor arm in the arms_pool.
        current_arm_index
            index of the current arm in the arms_pool.

        Returns
        -------
        int
            -1 means that calibrated preference estimate is less than lower bias.
            0 means that calibrated preference estimate is between lower and upper bias.
            1 means that calibrated preference estimate is greater than upper bias
        """
        rounds = int(
            8
            / math.pow((self.bias_upper - self.bias_lower), 2)
            * math.log(2 / self.confidence)
        )

        win_count_anchor = (
            0  # stores the number of times anchor arm beats current arm in the duel.
        )
        rounds_for_iteration = rounds  # the rounds variable is used for calculating the calibrated preference estimate.

        # loop for 'rounds_for_iteration' times so that an estimate is calculated.
        while (
            rounds_for_iteration > 0
        ):  # iterate the algorithm for multiple comparisons of the two arms.

            feedback_result = self.feedback(
                anchor_arm_index, current_arm_index
            )  # comparison of both the arms.
            if feedback_result == 1:
                win_count_anchor += 1
            rounds_for_iteration = rounds_for_iteration - 1

        calibrated_preference_estimate = (
            win_count_anchor / rounds
        ) - 0.5  # calculate the estimate.

        # return -1 if calibrated preference estimate is less than (3/4 * lower bias + 1/4 * upper bias).
        if calibrated_preference_estimate < (3 / 4) * (self.bias_lower) + (1 / 4) * (
            self.bias_upper
        ):
            return -1

        # return 1 if calibrated preference estimate is greater than (3/4 * upper bias + 1/4 * lower bias).
        elif calibrated_preference_estimate > (1 / 4) * (self.bias_lower) + (3 / 4) * (
            self.bias_upper
        ):
            return 1

        # return 0 if calibrated preference estimate is greater than lower bias but lower than upper bias.
        else:
            return 0


def _main() -> None:
    """Test the algorithm implementation."""
    given_preference_probability_matrix = np.array(
        [
            [0.5, 0.9, 0.9, 0.9],
            [0.1, 0.5, 0.75, 0.75],
            [0.1, 0.25, 0.5, 0.6],
            [0.1, 0.25, 0.4, 0.5],
        ]
    )

    def compare_feedback(anchor_arm_index: int, current_arm_index: int,) -> int:
        """Determine how the arms should be compared.

        Parameters
        ----------
        anchor_arm_index
            index of the anchor arm in the arms_pool.
        current_arm_index
            index of the current arm in the arms_pool.

        Returns
        -------
        int
            1 if the random value is less than the given preference probability for the two arms.
            -1 if the random value is greater than the given preference probability for the two arms.
        """
        given_probability_preference = given_preference_probability_matrix[
            anchor_arm_index, current_arm_index
        ]
        random_value = np.random.uniform()
        if random_value <= given_probability_preference:
            feedback_value = 1
        else:
            feedback_value = -1
        return feedback_value

    arm_pool = list()
    arm_pool.append("One")
    arm_pool.append("Two")
    arm_pool.append("Three")
    arm_pool.append("Four")

    # initialise the algorithm.
    seq = SequentialElimination(compare_feedback, 0.01, 0.012, 0.32)
    # run the algorithm.
    print(seq.find_winner_arms(arm_pool, "Two"))


if __name__ == "__main__":
    _main()
